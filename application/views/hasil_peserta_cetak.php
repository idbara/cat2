<link href='<?php echo base_url(); ?>___/css/style_print.css' rel='stylesheet' media='' type='text/css'/>
<title><?php echo $detil_tes->nama_ujian."-".$hasil->nama; ?></title>
<h3>Laporan Hasil Ujian</h3>
<hr style="border: solid 1px #000"><br>

<h4>Detil Ujian</h4>
<table class="table-bordered" style="margin-bottom: 0px">
  <tr><td>Nama Ujian</td><td width="70%"><b><?php echo $detil_tes->nama_ujian; ?></b></td></tr>
  <tr><td>Jumlah Soal</td><td><b><?php echo $detil_tes->jumlah_soal; ?></b></td></td></tr>
  <tr><td>Nama Peserta</td><td><b><?php echo $hasil->nama; ?></b></td></tr>
  <tr><td>Nilai</td><td><b><?php echo $hasil->nilai; ?></b></td></tr>
</table>
<br><br>
<h4>Review Ujian</h4>
<hr>
<table>
  
    <?php 
      if (!empty($soal)) {
        $no = 1;
        foreach ($list_jawaban as $l) {
          list($nomer_j,$jawaban) = explode(":", $l);
          foreach ($soal as $s) {
            
            if ($nomer_j == $s->id) {
              echo '<tr>
                <td>'.$no.'.</td>
                <td>'.trim($s->soal,"<p></p>").'</td>
                </tr>
                <tr>
                <table>';
                if ($jawaban == "A") {
                  echo '<tr><td><b>a. '.trim($s->opsi_a,"#####<p>").'</b></td></tr>';
                } else {
                  echo '<tr><td>a. '.trim($s->opsi_a,"#####<p>").'</td></tr>';
                }
                if ($jawaban == 'B') {
                  echo '<tr><td><b>b. '.trim($s->opsi_b,"#####<p>").'</b></td></tr>';
                } else {
                  echo '<tr><td>b. '.trim($s->opsi_b,"#####<p>").'</td></tr>';
                }
                if ($jawaban == 'C') {
                  echo '<tr><td><b>c. '.trim($s->opsi_c,"#####<p>").'</b></td></tr>';
                } else {
                  echo '<tr><td>c. '.trim($s->opsi_c,"#####<p>").'</td></tr>';
                }
                if ($jawaban == 'D') {
                  echo '<tr><td><b>d. '.trim($s->opsi_d,"#####<p>").'</b></td></tr>';
                } else {
                  echo '<tr><td>d. '.trim($s->opsi_d,"#####<p>").'</td></tr>';
                }
                if ($jawaban == "E") {
                  echo '<tr><td><b>e. '.trim($s->opsi_e,"#####<p>").'</b></td></tr>';
                } else {
                  echo '<tr><td>e. '.trim($s->opsi_e,"#####<p>").'</td></tr>';
                }
                  
                  echo'
                </table></td>
                </tr>
                ';
              if ($s->jawaban != $jawaban) {
                echo "<tr><td><div style='color : red;'>Salah! jawaban yang benar <b>".$s->jawaban."</b></div></td></tr><hr>";
              } else {
                echo "<tr><td><div style='color : green;'><b>Benar! </b></div></td></tr><hr>";
                
              }
            }
            
          }
        $no++;
        }
      } else {
        echo '<tr><td colspan="5">Belum ada data</td></tr>';
      }
    ?>
</table>
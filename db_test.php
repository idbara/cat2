<?php
  try {

    $pdo = new PDO(
    'mysql:dbname=cat2;host=cat2_mysql;charset=utf8',
    'root',
    '4UR4G4NT3N6',
    [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ]
   );

} catch (PDOException $e) {

   header('Content-Type: text/plain; charset=UTF-8', true, 500);
   exit($e->getMessage()); 

}
/*
 Navicat Premium Data Transfer

 Source Server         : cat2
 Source Server Type    : MySQL
 Source Server Version : 80031 (8.0.31)
 Source Host           : localhost:3306
 Source Schema         : cat2

 Target Server Type    : MySQL
 Target Server Version : 80031 (8.0.31)
 File Encoding         : 65001

 Date: 09/11/2022 23:07:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for m_admin
-- ----------------------------
DROP TABLE IF EXISTS `m_admin`;
CREATE TABLE `m_admin`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `level` enum('admin','guru','siswa') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kon_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `kon_id`(`kon_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 128 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_admin
-- ----------------------------
INSERT INTO `m_admin` VALUES (1, 'admin', '653a6aaa2c5d92ce323187d7764dc112', 'admin', 0);
INSERT INTO `m_admin` VALUES (126, '3328', 'e94fe9ac8dc10dd8b9a239e6abee2848', 'siswa', 181);
INSERT INTO `m_admin` VALUES (127, '3327', '20b02dc95171540bc52912baf3aa709d', 'siswa', 180);

-- ----------------------------
-- Table structure for m_guru
-- ----------------------------
DROP TABLE IF EXISTS `m_guru`;
CREATE TABLE `m_guru`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_guru
-- ----------------------------

-- ----------------------------
-- Table structure for m_mapel
-- ----------------------------
DROP TABLE IF EXISTS `m_mapel`;
CREATE TABLE `m_mapel`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_mapel
-- ----------------------------
INSERT INTO `m_mapel` VALUES (6, 'Latihan');

-- ----------------------------
-- Table structure for m_siswa
-- ----------------------------
DROP TABLE IF EXISTS `m_siswa`;
CREATE TABLE `m_siswa`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nim` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jurusan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 182 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_siswa
-- ----------------------------
INSERT INTO `m_siswa` VALUES (180, 'test1', '3327', 'KASI');
INSERT INTO `m_siswa` VALUES (181, 'test2', '3328', 'KAUR');

-- ----------------------------
-- Table structure for m_soal
-- ----------------------------
DROP TABLE IF EXISTS `m_soal`;
CREATE TABLE `m_soal`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_guru` int NULL DEFAULT NULL,
  `id_mapel` int NULL DEFAULT NULL,
  `bobot` int NULL DEFAULT NULL,
  `file` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipe_file` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `soal` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `opsi_a` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `opsi_b` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `opsi_c` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `opsi_d` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `opsi_e` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `jawaban` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_input` datetime NULL DEFAULT NULL,
  `jml_benar` int NULL DEFAULT NULL,
  `jml_salah` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_guru`(`id_guru` ASC) USING BTREE,
  INDEX `id_mapel`(`id_mapel` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 473 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_soal
-- ----------------------------
INSERT INTO `m_soal` VALUES (57, 0, 6, 1, '', '', '<p>Berapa jumlah desa di kabupaten pemalang?</p>\r\n', '#####<p>207</p>\r\n', '#####<p>208</p>\r\n', '#####<p>209</p>\r\n', '#####<p>210</p>\r\n', '#####<p>211</p>\r\n', 'E', '0000-00-00 00:00:00', 37, 51);
INSERT INTO `m_soal` VALUES (58, 0, 6, 1, '', '', '<p>Seseorang orang memiliki kekuasaan dalam hak pemerintahan dalam suatu wilayah desa disebut</p>\r\n', '#####<p>Preman</p>\r\n', '#####<p>Satpam</p>\r\n', '#####<p>Pangdam</p>\r\n', '#####<p>Kepala desa</p>\r\n', '#####<p>Lurah</p>\r\n', 'D', '0000-00-00 00:00:00', 71, 17);
INSERT INTO `m_soal` VALUES (59, 0, 6, 1, '', '', '<p>Berikut adalah nama kabupaten yang ada di Jawa Tengah, <strong>kecuali</strong></p>\r\n', '#####<p>Salatiga</p>\r\n', '#####<p>Boyolali</p>\r\n', '#####<p>Wonogiri</p>\r\n', '#####<p>Batang</p>\r\n', '#####<p>Grobogan</p>\r\n', 'A', '0000-00-00 00:00:00', 22, 66);
INSERT INTO `m_soal` VALUES (60, 0, 6, 1, '', '', '<p>Berikut yang merupakan nama Desa di Kabupaten Pemalang, <strong>kecuali</strong></p>\r\n', '#####<p>Cawet</p>\r\n', '#####<p>Iser</p>\r\n', '#####<p>Pener</p>\r\n', '#####<p>Sitemu</p>\r\n', '#####<p>Tanjungsari</p>\r\n', 'E', '0000-00-00 00:00:00', 20, 66);
INSERT INTO `m_soal` VALUES (61, 0, 6, 1, '', '', '<p>Dibawah ini yang merupakan Wakil Presiden pada masa pemerintahan Presiden Soeharto, <strong>kecuali</strong></p>\r\n', '#####<p>Adam Malik</p>\r\n', '#####<p>Soedharmono</p>\r\n', '#####<p>Try Sutrisno</p>\r\n', '#####<p>Hamzah Haz</p>\r\n', '#####<p>Umar Wirahadikumah</p>\r\n', 'D', '0000-00-00 00:00:00', 33, 53);
INSERT INTO `m_soal` VALUES (471, 0, 6, 1, NULL, NULL, '<p>Uji Coba Input Soal</p>\n', '#####<p>Jawab a</p>\n', '#####<p>b</p>\n', '#####<p>c</p>\n', '#####<p>d</p>\n', '#####<p>e</p>\n', 'A', NULL, NULL, NULL);
INSERT INTO `m_soal` VALUES (472, 0, 6, 1, NULL, NULL, '<p>1+1</p>\n', '#####<p>2</p>\n', '#####<p>3</p>\n', '#####<p>4</p>\n', '#####<p>5</p>\n', '#####<p>6</p>\n', 'A', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tr_guru_mapel
-- ----------------------------
DROP TABLE IF EXISTS `tr_guru_mapel`;
CREATE TABLE `tr_guru_mapel`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_guru` int NOT NULL,
  `id_mapel` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_guru`(`id_guru` ASC) USING BTREE,
  INDEX `id_mapel`(`id_mapel` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tr_guru_mapel
-- ----------------------------

-- ----------------------------
-- Table structure for tr_guru_tes
-- ----------------------------
DROP TABLE IF EXISTS `tr_guru_tes`;
CREATE TABLE `tr_guru_tes`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_guru` int NOT NULL,
  `id_mapel` int NOT NULL,
  `nama_ujian` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah_soal` int NOT NULL,
  `waktu` int NOT NULL,
  `jenis` enum('acak','set') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `detil_jenis` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `terlambat` datetime NOT NULL,
  `token` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_guru`(`id_guru` ASC) USING BTREE,
  INDEX `id_mapel`(`id_mapel` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tr_guru_tes
-- ----------------------------
INSERT INTO `tr_guru_tes` VALUES (20, 0, 6, 'Latihan', 5, 10, 'acak', '', '2022-11-09 22:00:00', '2022-11-10 20:00:00', 'UQEGM');

-- ----------------------------
-- Table structure for tr_ikut_ujian
-- ----------------------------
DROP TABLE IF EXISTS `tr_ikut_ujian`;
CREATE TABLE `tr_ikut_ujian`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_tes` int NOT NULL,
  `id_user` int NOT NULL,
  `list_soal` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `list_jawaban` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jml_benar` int NOT NULL,
  `nilai` decimal(10, 2) NOT NULL,
  `nilai_bobot` decimal(10, 2) NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `tgl_selesai` datetime NOT NULL,
  `status` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_tes`(`id_tes` ASC) USING BTREE,
  INDEX `id_user`(`id_user` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tr_ikut_ujian
-- ----------------------------
INSERT INTO `tr_ikut_ujian` VALUES (31, 20, 180, '61,57,59,60,58', '61:C:N,57:C:N,59:A:N,60:D:N,58:D:N', 2, 40.00, 40.00, '2022-11-09 22:28:02', '2022-11-09 22:38:02', 'N');
INSERT INTO `tr_ikut_ujian` VALUES (32, 20, 181, '471,58,57,59,472', '471:B:N,58:D:N,57:D:N,59:E:N,472:A:N', 2, 40.00, 40.00, '2022-11-09 22:59:11', '2022-11-09 23:09:11', 'N');

-- ----------------------------
-- Table structure for tr_siswa_ujian
-- ----------------------------
DROP TABLE IF EXISTS `tr_siswa_ujian`;
CREATE TABLE `tr_siswa_ujian`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_siswa` int NOT NULL,
  `id_ujian` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_siswa`(`id_siswa` ASC) USING BTREE,
  INDEX `id_ujian`(`id_ujian` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tr_siswa_ujian
-- ----------------------------
INSERT INTO `tr_siswa_ujian` VALUES (8, 180, 20);
INSERT INTO `tr_siswa_ujian` VALUES (9, 181, 20);

-- ----------------------------
-- Triggers structure for table m_guru
-- ----------------------------
DROP TRIGGER IF EXISTS `hapus_guru`;
delimiter ;;
CREATE TRIGGER `hapus_guru` AFTER DELETE ON `m_guru` FOR EACH ROW BEGIN
DELETE FROM m_soal WHERE m_soal.id_guru = OLD.id;
DELETE FROM m_admin WHERE m_admin.level = 'guru' AND m_admin.kon_id = OLD.id;
DELETE FROM tr_guru_mapel WHERE tr_guru_mapel.id_guru = OLD.id;
DELETE FROM tr_guru_tes WHERE tr_guru_tes.id_guru = OLD.id;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table m_mapel
-- ----------------------------
DROP TRIGGER IF EXISTS `hapus_mapel`;
delimiter ;;
CREATE TRIGGER `hapus_mapel` AFTER DELETE ON `m_mapel` FOR EACH ROW BEGIN
DELETE FROM m_soal WHERE m_soal.id_mapel = OLD.id;
DELETE FROM tr_guru_mapel WHERE tr_guru_mapel.id_mapel = OLD.id;
DELETE FROM tr_guru_tes WHERE tr_guru_tes.id_mapel = OLD.id;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table m_siswa
-- ----------------------------
DROP TRIGGER IF EXISTS `hapus_siswa`;
delimiter ;;
CREATE TRIGGER `hapus_siswa` AFTER DELETE ON `m_siswa` FOR EACH ROW BEGIN
DELETE FROM tr_ikut_ujian WHERE tr_ikut_ujian.id_user = OLD.id;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
